import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'custom-utenti',
  templateUrl: './utenti.component.html',
  styleUrls: ['./utenti.component.css']
})
export class UtentiComponent implements OnInit {

  constructor(public router: Router) { }

  openAggiungiUtente() {
    this.router.navigate(['./home/aggiungi-utente'])
  }

  ngOnInit() {
  }

}
