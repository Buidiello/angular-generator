import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'custom-aggiungi-utente',
  templateUrl: './aggiungi-utente.component.html',
  styleUrls: ['./aggiungi-utente.component.css']
})
export class AggiungiUtenteComponent implements OnInit {

  constructor(public router: Router) { }

 goBack() {
    this.router.navigate(['./home/utenti'])
  }

  ngOnInit() {
  }

}
