import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
/*import axios from 'axios';
import server from '../../config/server';
import {MatDialog} from '@angular/material';
import {ModalConfirmedComponent} from '../modal-confirmed/modal-confirmed.component';*/

const config = {
  headers: {'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json'}
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);

  Email: string | undefined;
  Password: string | undefined;
  login_fail = false;

  constructor(private router: Router) {
  }

  login() {
    this.router.navigate(['/home']);
   /* console.log('login');
    const self = this;
    axios.post(server.API_URL + '/utenti/login', {'email': this.Email, 'password': this.Password}, config)
      .then(function (response) {
        if (response.status == 200) {
          localStorage.setItem('User', JSON.stringify(response.data));
          const token = JSON.parse(localStorage.getItem('User')).id;
          if (token != undefined) {
            const userId = JSON.parse(localStorage.getItem('User')).userId;
            axios.get(server.API_URL + `/utenti/${userId}`, {
              headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'x-access-token': JSON.parse(localStorage.getItem('User')).id
              }
            })
              .then(function (res) {
                console.log(res);
                localStorage.setItem('Anagrafica', JSON.stringify(res.data));
                self.router.navigate(['/home/pratiche']);
              })
              .catch(function (error) {
                console.log(error);
              });
          }
        }
        /* if (response.status == 449) {
          alert('test 449');
          const dialogRef = self.dialog.open(ModalConfirmedComponent, {
            width: '400px'
          });
          dialogRef.componentInstance.testo_figlio = 'E\' necessario confermare la registrazione prima di effettuare il login';
        } 
      })
      .catch(function (error) {
        console.log(error);
        self.login_fail = true;
        self.Email = '';
        self.Password = '';
      });*/
  }

  ngOnInit() {}
}
