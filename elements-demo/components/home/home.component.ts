import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  roleId: number;

  constructor(public router: Router) {
   // this.roleId = JSON.parse(localStorage.getItem('Anagrafica')).role_id;
  }

  logout() {
    console.log('logout');
    this.router.navigate(['/']);
  }

  ngOnInit() {
  }

}
