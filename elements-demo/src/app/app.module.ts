import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {  Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './material';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { UtentiComponent } from './utenti/utenti.component';
import { StatisticheComponent } from './statistiche/statistiche.component';
import { ChartsModule } from 'ng2-charts';
import { ProfiloComponent } from './profilo/profilo.component';
import { AggiungiUtenteComponent } from './aggiungi-utente/aggiungi-utente.component';
import { MappaComponent } from './mappa/mappa.component';
import { AgmCoreModule } from '@agm/core';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: 'utenti',
        component: UtentiComponent,
      },
      {
        path: 'statistiche',
        component: StatisticheComponent,
      },
      {
        path: 'profilo',
        component: ProfiloComponent,
      },
      {
        path: 'aggiungi-utente',
        component: AggiungiUtenteComponent,
      },
      {
        path: 'mappa',
        component: MappaComponent,
      },
      {
        path: 'rich-text-editor',
        component: RichTextEditorComponent,
      },
    ]
  },
  
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    UtentiComponent,
    StatisticheComponent,
    ProfiloComponent,
    AggiungiUtenteComponent,
    MappaComponent,
    RichTextEditorComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    DemoMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    ChartsModule,
    CKEditorModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCd_2W9F7gY81NnbpVuJssrO_gPxFRJ0DQ'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
