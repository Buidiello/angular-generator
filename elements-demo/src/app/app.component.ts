import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'custom-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'elements-demo';
  defaultPage: String = '';
  constructor(public router: Router) {
    this.router.navigate([this.defaultPage]);
  }
}
