#!/usr/bin/env node
"use strict";
var chalk = require('chalk');
var clear = require('clear');
var figlet = require('figlet');
var path = require('path');
var program = require('commander');
clear();
console.log(chalk.red(figlet.textSync('STDOUT', { horizontalLayout: 'full' })));
program
    .version('0.0.1')
    .description("Angular generator template")
    .option('-s, --sidebar', 'Add sidebar')
    .option('-h, --header', 'Add header')
    .command('mkdir test')
    .option('-r, --recursive', 'Remove recursively')
    .action(function (dir, cmd) {
    console.log('remove ' + dir + (cmd.recursive ? ' recursively' : ''));
})
    .parse(process.argv)
    .parse(process.argv);
/*program
.command('mkdir test')
.option('-r, --recursive', 'Remove recursively')
.action(function (dir, cmd) {
  console.log('remove ' + dir + (cmd.recursive ? ' recursively' : ''))
})
.parse(process.argv)*/
console.log('you set:');
if (program.sidebar)
    console.log('  - sidebar');
if (program.header)
    console.log('  - header');
