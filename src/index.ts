#!/usr/bin/env node

const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
const path = require('path');
const program = require('commander');
const shell = require('shelljs');
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});
const fs = require('fs');

clear();
console.log(
  chalk.red(
    figlet.textSync('STDOUT', { horizontalLayout: 'full' })
  )
);


function readWriteSync(page) {
  var data = fs.readFileSync('./template/elements-demo/src/app/app.component.ts', 'utf-8');

  
  var newValue = data.replace(`defaultPage: String = '';`, `defaultPage: String = \'${page}\';`);

  console.log('data: ', data)

  fs.writeFileSync('./template/elements-demo/src/app/app.component.ts', newValue, 'utf-8');

  console.log('readFileSync complete');
}

program
  .version('0.0.1')
  .description("Angular generator template")
  .option('-s, --sidebar', 'Add sidebar')
  .option('-h, --header', 'Add header')
  .parse(process.argv)
  .parse(process.argv);

console.log('you set:');
if (program.sidebar) console.log('  - sidebar');
if (program.header) console.log('  - header')

//Create new folder
//shell.cd('..');
/*shell.ls('*').forEach(function (file) {
  console.log('File: ', file)
});*/


shell.mkdir('-p', 'template', 'template');
//Copy template project
//shell.rm('-rf', 'template');
let result = shell.cp('-R', './elements-demo', './template').code;
console.log('result: ', result)
if (result == 0) {
  shell.echo('Project correctly copied!');

  //Choose options
  readline.question(`Do you want the login page? [y: yes - n: no]`, (response) => {
  console.log(`Your response ${response}!`);
  readline.close()
  if(response != 'y') {
    readWriteSync('home');
    shell.cd('./template/elements-demo');
    let result = shell.exec('ng serve --open').code;
    console.log('result: ', result)
    if (result!== 0) {
      shell.echo('Project correctly started!');
      shell.exit(1);
    } 
  } 
})

  //hell.exit(1);
} else {
  shell.exit(1)
}
/*let result = shell.exec('ng new elements-demo --prefix custom').code;
console.log('result: ', result)
if (result!== 0) {
  shell.echo('Project correctly copied!');
  shell.exit(1);
} */


